# for loading/processing the images  
from keras.preprocessing.image import load_img 
from keras.preprocessing.image import img_to_array 
# from keras.applications.vgg19 import preprocess_input 
# from keras.applications.resnet50 import preprocess_input

# models 
from keras.applications.vgg16 import VGG16 
from keras.applications.vgg19 import VGG19
from keras.applications.resnet50 import ResNet50
from keras.applications.efficientnet import EfficientNetB0, preprocess_input

from keras.models import Model
# clustering and dimension reduction
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


# for everything else
import shutil,glob
import os
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import pandas as pd
import pickle,time

### INPUTS
G=4

#path=os.getcwd()+'/n/'
path='/home/user/Documents/CLUSTER/march_2to7_poa/'
#### INPUTS

dest=os.getcwd()+'/'

print()
print('No. of Images: ',len(os.listdir(path)))    # No. of images count.
print()
os.chdir(path)   # change the working directory to the path where the images are located

flowers = []          # this list holds all the image filename

# creates a ScandirIterator aliased as files
with os.scandir(path) as files:
  # loops through each file in the directory
    for file in files:
        if file.name.endswith('.jpg'):
          # adds only the image files to the flowers list
            flowers.append(file.name)
         
# model = VGG19()
# model = Model(inputs = model.inputs, outputs = model.layers[-2].output)
# model = ResNet50()
# model = Model(inputs=model.input, outputs=model.layers[-2].output)
model = EfficientNetB0(weights='imagenet')
model = Model(inputs=model.input, outputs=model.layers[-2].output)

def extract_features(file, model):       
    img = load_img(file, target_size=(224,224))        # load the image as a 224x224 array
    img = np.array(img)                               # convert from 'PIL.Image.Image' to numpy array
    reshaped_img = img.reshape(1,224,224,3)         # reshape the data for the model reshape(num_of_samples, dim 1, dim 2, channels)
    # prepare image for model
    imgx = preprocess_input(reshaped_img)
    # get the feature vector
    features = model.predict(imgx, use_multiprocessing=True)
    return features
start_time = time.time()  
print(start_time)
data = {}
p = r"flower_features.pkl"
c=0
for flower in flowers:           # lop through each image in the dataset
    # try to extract the features and update the dictionary
    try:
        feat = extract_features(flower,model)
        data[flower] = feat
    # if something fails, save the extracted features as a pickle file (optional)
    except:
        with open(p,'wb') as file:
            pickle.dump(data,file)
    c=c+1
    if str(c).endswith('0'):
        print(c)
          
filenames = np.array(list(data.keys()))      #list of the filenames

feat = np.array(list(data.values()))      # list of just the features
print()        
print(feat.shape)
feat = feat.reshape(-1,feat.shape[2])  
# feat = feat.reshape(-1,4096)                 # reshape so that there are 210 samples of 4096 vectors

label = [i for i in range(G)]
unique_labels = list(set(label))

# reduce the amount of dimensions in the feature vector
pca = PCA()
pca.fit(feat)
x = pca.transform(feat)

# Assuming you've already prepared your data and called it 'x'
# If not, replace 'x' with your data
wcss = []  # Within-Cluster-Sum-of-Squares
# Try different values of k from 1 to a maximum value, e.g., 10
# for i in range(1, 41):
#     kmeans = KMeans(n_clusters=i, random_state=22)
#     kmeans.fit(x)  # Your data, 'x'
#     wcss.append(kmeans.inertia_)

# # Plot the Elbow Method graph
# plt.plot(range(1, 41), wcss)
# plt.title('Elbow Method for Optimal K')
# plt.xlabel('Number of clusters (K)')
# plt.ylabel('Within-Cluster-Sum-of-Squares (WCSS)')
# print('______________________________________________________')
# print('----Observe plot, choose K value from elbow method----')
# plt.show()


print()
# K=(input('Enter K value-->> '))
K=G
# try:
#     K=int(K)
# except:
#     pass
# if isinstance(K,int)==True:
#     pass
# else:
#     K=int(input('Plese enter a numeric value-->> '))
print()
for i in range(K):
    i=str(i)
    if os.path.isdir(dest+'/G'+i):
        pass
    else:
        os.mkdir(dest+'/G'+i)
        print('/G'+i,' folder created')

# cluster feature vectors
kmeans = KMeans(n_clusters=K, random_state=22)
kmeans.fit(x)

# holds the cluster id and the images { id: [images] }
groups = {}
for file, cluster in zip(filenames,kmeans.labels_):
    if cluster not in groups.keys():
        groups[cluster] = []
        groups[cluster].append(file)
    else:
        groups[cluster].append(file)
### move images w.r.t. clusters
for k in range(len(label)):
    try:
        for i in groups[k]:
            # print(dest+'/grp'+str(k)+'/')
            shutil.copy(path+i,dest+'/G'+str(k)+'/') 
    except:
        print('err')

end_time = time.time()
print('No. of samples: ',len(os.listdir(path))) 
print('Time Taken: ',end_time-start_time)
print()
print('Done_Hero')
